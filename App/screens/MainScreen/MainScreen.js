import React, { Component } from "react";
import MapboxGL from '@mapbox/react-native-mapbox-gl'

import {
    StyleSheet,
    View,
    Dimensions,
    TouchableOpacity,
    Text,
    Platform,
    PermissionsAndroid,
    FlatList,
    Image
} from "react-native";

import BottomSheet from 'reanimated-bottom-sheet'
import ActionButton from 'react-native-action-button'
import EIcon from 'react-native-vector-icons/Entypo'
import MIcon from 'react-native-vector-icons/MaterialIcons'
import Geolocation from 'react-native-geolocation-service';
import Button from 'react-native-flat-button'


const MYAPIKEY = 'pk.eyJ1IjoibWF5ZXJzcGl0eiIsImEiOiJjanlxMjBvb2MxbHJoM21xdXNyYnlzbGFmIn0.MHPd3fG8zeecgMCdYipzkA'
const TAG = "SocialMap:MainScreen"
const SCREEN_WIDTH = Dimensions.get('window').width
const SCREEN_HEIGHT = Dimensions.get('window').height
const BOTTOMSHEET_HEIGHT_INIT = 100

const coordinates = [
    [-73.98330688476561, 40.76975180901395],
    [-73.98330688476561, 43.86975180901395],
    [-70.88330688476561, 40.76975180901395]
]
const locationName = [
    { 'key': "Street1" },
    { 'key': "Street2" },
    { 'key': "Street3" }
]

MapboxGL.setAccessToken(
    MYAPIKEY
)

const messageTemplateArray = [
    {
        'image': 'https://randomuser.me/api/portraits/men/1.jpg',
        'customer': 'jeremylew',
        'content': 'Hey man, can we get this over with?'
    },
    {
        'image': 'https://randomuser.me/api/portraits/men/86.jpg',
        'customer': 'Jeremy Lew',
        'content': 'Hey man, can we get this over with?'
    },
    {
        'image': 'https://randomuser.me/api/portraits/men/64.jpg',
        'customer': 'Jeremy Kaufman',
        'content': 'Hey man, can we get this over with?'
    }
]

class MainScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isMessageBtn: true,
            LocationBtnState: 2,
            coordinates: coordinates,
            locationName: locationName,
            currentGeo: [-73.970895, 40.723279],
            isPostLocation: false,
            isLocationOrMessage: 0,
            buttomComponent: BOTTOMSHEET_HEIGHT_INIT,
            myCurrentAddress: 'New Street'
        };

    }

    componentDidMount() {
        if (Platform.OS !== "ios") {
            this._requestLocationPermission();
        }
    }



    render() {
        return (
            <View style={styles.rootConatiner}>
                <View style={{ height: "100%", width: '100%' }}>
                    <View style={{ flex: 1, width: SCREEN_WIDTH }}>
                        <MapboxGL.MapView
                            zoomLevel={9}
                            pitch={45}
                            centerCoordinate={this.state.currentGeo}
                            ref={ref => (this.map = ref)}
                            style={{ flex: 1, width: SCREEN_WIDTH }}
                            onPress={() => this.renderMapViewRefresh(2, BOTTOMSHEET_HEIGHT_INIT, false, 2, 0)}
                        >
                            {this.renderAnnotations()}
                        </MapboxGL.MapView>
                        <Button
                            type="primary"
                            onPress={() => {
                                Geolocation.getCurrentPosition(
                                    (position) => {
                                        console.log(TAG, position.coords)
                                        this.map.flyTo([position.coords.longitude, position.coords.latitude], 2500);
                                    },
                                    (error) => {
                                        // See error code charts below.
                                        console.log(error.code, error.message);

                                        this.ErrorAlertWithCurrentPosition(error.message)
                                    },
                                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                                );
                            }}
                            containerStyle={styles.buttonContainer}
                        >
                            <MIcon name="my-location" style={styles.actionButtonIcon} />
                        </Button>

                        {/* Rest of the app comes ABOVE the action button component !*/}
                        <ActionButton buttonColor="rgba(231,76,60,1)" style={styles.actionButton}>
                            <ActionButton.Item buttonColor='#9b59b6' title="Send a Direct Message" onPress={() => {
                                console.log(TAG, "Direct Message tapped!")
                            }}>
                                <EIcon name="message" style={styles.actionButtonIcon} />
                            </ActionButton.Item>
                            <ActionButton.Item buttonColor='#3498db' title="Post Location" onPress={() => {
                                console.log(TAG, "Post Location tapped!")

                                Geolocation.getCurrentPosition(
                                    (position) => {
                                        console.log(TAG, position.coords)
                                        if (!this.state.isPostLocation) {
                                            let temp = this.state.coordinates;
                                            temp = temp.concat([[position.coords.longitude, position.coords.latitude]]);
                                            let temp_name = this.state.locationName;
                                            console.log(TAG, "currentPosAddress -> ", this.state.myCurrentAddress)
                                            temp_name = temp_name.concat([{ 'key': this.state.myCurrentAddress }]);
                                            this.setState({
                                                coordinates: temp,
                                                locationName: temp_name,
                                                isPostLocation: true
                                            });
                                            this.map.flyTo([position.coords.longitude, position.coords.latitude], 2500);
                                        } else {
                                            alert("Your location already posted.");
                                        }

                                    },
                                    (error) => {
                                        // See error code charts below.
                                        console.log(error.code, error.message);
                                        this.ErrorAlertWithCurrentPosition(error.message)
                                    },
                                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                                );
                            }}>
                                <EIcon name="location-pin" style={styles.actionButtonIcon} />
                            </ActionButton.Item>
                        </ActionButton>
                    </View>
                    <View style={{ height: this.state.buttomComponent, width: '100%' }} />
                </View>

                <BottomSheet
                    ref={this.bs}
                    snapPoints={[SCREEN_HEIGHT, SCREEN_HEIGHT / 2, BOTTOMSHEET_HEIGHT_INIT]}
                    renderContent={this.renderInner}
                    renderHeader={this.renderHeader}
                    initialSnap={2}
                />

            </View>
        );
    }

    renderList = () => {
        if (this.state.isLocationOrMessage == 1) {
            return (
                <FlatList
                    data={this.state.locationName}
                    extraData={this.state.locationName}
                    renderItem={(item) => (

                        <TouchableOpacity
                            onPress={() => this.onPressLocationItem(item.index)}
                        >
                            <View style={styles.myLocationFlatList}>
                                <EIcon name="location-pin" style={styles.FlatListItemIcon} />
                                <Text style={{ marginLeft: 12 }}>{item.item.key}</Text>
                            </View>
                            <View style={styles.myListLineView}>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            );
        } else {
            return (
                <FlatList
                    data={messageTemplateArray}
                    renderItem={(item) => (
                        <View
                        >
                            <View style={styles.myMessageList}>
                                <Image
                                    source={{ uri: item.item.image }}
                                    style={styles.FlatListItemAvatar}
                                />
                                <View style={{ marginLeft: 12 }}>
                                    <Text style={{ fontWeight: '600' }}>{item.item.customer}</Text>
                                    <Text>{item.item.content}</Text>
                                </View>
                            </View>
                            <View style={styles.myListLineView}>
                            </View>
                        </View>
                    )}
                />
            );
        }
    }

    renderInner = () => (
        <View style={styles.panel}>
            <View style={{ width: '100%', height: 600 }}>
                {this.renderList()}
            </View>
        </View>
    )

    renderMapViewRefresh = (pos, screenHeight, isMessageBtn, locationBtnState, isLocationOrMessage) => {

        console.log(TAG, locationBtnState, " ", isMessageBtn, " ", isLocationOrMessage)

        this.bs.current.snapTo(pos)
        this.setState({
            buttomComponent: screenHeight,
            isMessageBtn: isMessageBtn,
            LocationBtnState: locationBtnState,
            isLocationOrMessage: isLocationOrMessage,
            coordinates: this.state.coordinates
        })
    }

    renderHeader = () => (
        <View style={styles.header}>
            <TouchableOpacity
                onPress={() => {
                    console.log(TAG, "locationbtnstate -> " + this.state.LocationBtnState)

                    switch (this.state.LocationBtnState) {
                        case 0:
                            this.renderMapViewRefresh(0, SCREEN_HEIGHT, false, 1, 1)
                            break
                        case 1:
                            if (this.state.isLocationOrMessage == 1) {
                                this.renderMapViewRefresh(2, 100, true, 2, 0)
                            }
                            else {
                                this.renderMapViewRefresh(0, SCREEN_HEIGHT, false, 1, 1)
                            }
                            break
                        case 2:
                            this.renderMapViewRefresh(1, SCREEN_HEIGHT / 2, true, 0, 1)
                            break
                        default:
                            console.log(TAG, "location_button_click error")
                    }

                }}
            >
                {this.state.isLocationOrMessage == 1 ? (
                    <View style={{ alignItems: "center" }}>
                        <EIcon name="location-pin" style={styles.TouchableOpacityIcon_1} />
                        <Text style={{ color: "blue" }}>Location</Text>
                    </View>
                )
                    : (
                        <View style={{ alignItems: "center" }}>
                            <EIcon name="location-pin" style={styles.TouchableOpacityIcon_2} />
                            <Text style={{ color: "gray" }}>Location</Text>
                        </View>

                    )}
            </TouchableOpacity>
            <TouchableOpacity style={{ alignItems: "center" }}
                onPress={() => {
                    console.log(TAG, "messageBtnState -> " + this.state.isMessageBtn)
                    if (this.state.LocationBtnState == 0) {
                        this.renderMapViewRefresh(0, SCREEN_HEIGHT, false, 1, 2)
                    }
                    else {
                        if (this.state.isMessageBtn) {
                            this.renderMapViewRefresh(0, SCREEN_HEIGHT, false, 1, 2)
                        } else {
                            if (this.state.isLocationOrMessage == 2) {
                                this.renderMapViewRefresh(2, 100, true, 2, 0)
                            } else {
                                this.renderMapViewRefresh(0, SCREEN_HEIGHT, false, 1, 2)
                            }
                        }
                    }

                }}
            >
                {this.state.isLocationOrMessage == 2 ? (
                    <View style={{ alignItems: "center" }}>
                        <EIcon name="message" style={styles.TouchableOpacityIcon_1} />
                        <Text style={{ color: "blue" }}>Message</Text>
                    </View>
                )
                    : (
                        <View style={{ alignItems: "center" }}>
                            <EIcon name="message" style={styles.TouchableOpacityIcon_2} />
                            <Text style={{ color: "gray" }}>Message</Text>
                        </View>

                    )}
            </TouchableOpacity>
        </View>
    )

    bs = React.createRef();

    renderAnnotation(counter) {
        const id = `pointAnnotation${counter}`;
        const coordinate = this.state.coordinates[counter];
        const title = `Longitude: ${this.state.coordinates[counter][0]} Latitude: ${this.state.coordinates[counter][1]}`;

        return (
            <MapboxGL.PointAnnotation
                key={id}
                id={id}
                title='Test'
                coordinate={coordinate}>
            </MapboxGL.PointAnnotation>
        );
    }

    renderAnnotations() {
        const items = [];

        for (let i = 0; i < this.state.coordinates.length; i++) {
            items.push(this.renderAnnotation(i));
        }

        return items;
    }

    onPressLocationItem = (index) => {
        this.map.flyTo([this.state.coordinates[index][0], this.state.coordinates[index][1]], 2500);
    }

    ErrorAlertWithCurrentPosition = (message) => {
        alert(message)
    }

    async _requestLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: "Access Location",
                    message:
                        "App needs to know your location"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can receive Location Now");

                Geolocation.getCurrentPosition(
                    (position) => {
                        this.setState({
                            currentGeo: [position.coords.longitude, position.coords.latitude]
                        })
                        console.log(TAG, position)

                        fetch('https://api.mapbox.com/geocoding/v5/mapbox.places/chester.json?proximity=' + this.state.currentGeo[0] + ',' + this.state.currentGeo[1] + '&access_token=' + MYAPIKEY)
                            .then((response) => response.json())
                            .then((responseJson) => {
                                console.log(TAG, 'ADDRESS GEOCODE is BACK!! => ' + JSON.stringify(responseJson));
                                console.log(TAG, 'PLACE NAME => ' + responseJson["features"][0]["place_name"])
                                this.setState({
                                    myCurrentAddress: responseJson["features"][0]["place_name"]
                                })
                            })

                    },
                    (error) => {
                        // See error code charts below.
                        console.log(error.code, error.message);

                        this.ErrorAlertWithCurrentPosition(error.message)
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                );
            } else {
                console.log("Get Location permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    }
}

const styles = StyleSheet.create({
    rootConatiner: {
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
        backgroundColor: 'white'
    },
    actionButton: {
        marginBottom: 60
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
    FlatListItemIcon: {
        fontSize: 20,
        height: 22,
        color: 'blue',
    },
    buttonContainer: {
        width: 50,
        height: 50,
        marginVertical: 5,
        borderRadius: 25,
        right: 32,
        bottom: 20,
        zIndex: 2000,
        position: 'absolute'
    },
    TouchableOpacityIcon_1: {
        fontSize: 20,
        height: 22,
        color: 'blue',
        justifyContent: "center"
    },
    TouchableOpacityIcon_2: {
        fontSize: 20,
        height: 22,
        color: 'gray',
        justifyContent: "center"
    },
    panel: {
        width: Dimensions.get("window").width,
        height: "100%",
        padding: 20,
        backgroundColor: 'rgb(249,249,249)',
    },
    header: {
        width: Dimensions.get("window").width,
        height: 50,
        backgroundColor: 'rgb(249,249,249)',
        shadowColor: '#000000',
        paddingTop: 20,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        flexDirection: 'row',
        justifyContent: 'space-around',
        zIndex: 1000
    },
    panelHeader: {
        alignItems: 'center',
    },
    panelHandle: {
        width: 40,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#00000040',
        marginBottom: 10,
    },
    FlatListItemAvatar: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        borderWidth: 1,
        borderColor: 'green'
    },
    myLocationFlatList: {
        width: '100%',
        height: 50,
        backgroundColor: 'rgb(255,255,255)',
        paddingLeft: 8,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    myListLineView: {
        width: '100%',
        height: 1,
        backgroundColor: 'rgb(240,240,240)'
    },
    myMessageList: {
        width: '100%',
        height: 64,
        backgroundColor: 'rgb(255,255,255)',
        paddingLeft: 8,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    }
});

export default MainScreen;