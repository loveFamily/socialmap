import {
    createAppContainer,
    createStackNavigator,
} from 'react-navigation';

import MainScreen from "./screens/MainScreen/MainScreen";

const MainStack = createStackNavigator(
    {
        MainScreen: {
            screen: MainScreen,
            navigationOptions: {
                header: null,
                gestureEnabled: false
            }
        }
    },
    {
        initialRouteName: 'MainScreen'
    }
);

export default createAppContainer(MainStack);
